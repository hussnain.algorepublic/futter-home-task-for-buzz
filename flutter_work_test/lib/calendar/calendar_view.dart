import 'package:flutter/material.dart';
import 'package:flutter_work_test/calendar/calendar_controller.dart';
import 'package:flutter_work_test/helper/common.dart';
import 'package:flutter_work_test/helper/text_styles.dart';
import 'package:mvc_application/view.dart' show StateMVC;
import 'package:flutter_work_test/helper/global.dart' as globals;

class CalendarView extends StatefulWidget {
  @override
  _CalendarViewState createState() => _CalendarViewState();
}

class _CalendarViewState extends StateMVC<CalendarView> {

  /// {MVC} Calendar controller
  CalendarController calendarController;

  /// controller for note edit text
  TextEditingController noteController = TextEditingController();


  Size size;

  /// Constructor
  _CalendarViewState() : super(CalendarController()) {
    calendarController = controller as CalendarController;
    getData();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: globals.backgroundColor,
        appBar: AppBar(
          title: const Text("Flutter Work Task"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Container(
                margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Column(
                  children: [
                    /// note attached
                    getNoteWidget(),

                    /// Year and month title
                    getMonthTitleWidget(),

                    /// Week title
                    getWeekTitleWidget(),

                    /// Calendar Widget
                    getCalendarWidget()
                  ],
                ))));
  }


  /// Note widget
  Widget getNoteWidget(){
    return Container(
      width: size.width,
      height: size.height / 3,
      child: Card(
          elevation: 20,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          color: globals.themColor,
          child: Container(
              padding: EdgeInsets.symmetric(
                  vertical: 20, horizontal: 20),
              child: TextField(
                controller: noteController,
                decoration: InputDecoration(
                    hintText: 'Note',
                    border: InputBorder.none,
                    hintStyle: TextStyle(
                        color: globals.backgroundColor)),
                style: TextStyles.getNoteTextStyle(
                    globals.backgroundColor),
                maxLines: 9,
                maxLength: null,
              ))),
    );
  }

  /// Month title widget
  Widget getMonthTitleWidget(){
    return Container(
      width: size.width,
      height: globals.heightOfGridViewRow,
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      child: Text(
        calendarController.getYearAndMonth(),
        style: TextStyles.getTitleStyle(globals.blackColor),
      ),
    );
  }

  /// Week title widget
  Widget getWeekTitleWidget() {
    return Container(
        width: size.width,
        height: globals.heightOfGridViewRow,
        decoration: BoxDecoration(
            color: globals.weekListBackgroundColor,
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: Common.getWeekList().length,
            mainAxisSpacing: 2.0,
            crossAxisSpacing: 2.0,
            childAspectRatio: 1.0,
          ),
          itemCount: Common.getWeekList().length,
          // padding: EdgeInsets.all(10),
          itemBuilder: (context, index) {
            return Container(
              height: globals.heightOfGridViewRow,
                padding: EdgeInsets.symmetric(vertical: 15),
                alignment: Alignment.topCenter,
                child: Text(
                  Common.getWeekList().elementAt(index),
                  style: TextStyles.getTitleStyle(globals.blackColor),
                ));
          },
        ));
  }


  /// Calendar widget
  Widget getCalendarWidget() {
    return Container(
        width: size.width,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 7,
            mainAxisSpacing: 2.0,
            crossAxisSpacing: 2.0,
            childAspectRatio: 1.0,
          ),
          itemCount: 42,
          // padding: EdgeInsets.all(10),
          itemBuilder: (context, index) {
            return InkWell(
                child: Container(
                    decoration: BoxDecoration(
                        color: calendarController.getCalendarBlockColor(index),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    alignment: Alignment.center,
                    child: Text(
                      calendarController.getCalendarModel(index).day.toString(),
                      style: TextStyles.getDaysStyle(
                          calendarController.getCalendarStyleColor(index)),
                    )
                ),
              ///  Date Click listener
              onTap: (){
                  calendarController.setSelectedIndex(index);
                  setState(() { });
              },
            );
          },
        ));
  }

  /// get data according to current month and year
  void getData() async {
    calendarController.setCalendarList();
    setState(() {});
  }
}
