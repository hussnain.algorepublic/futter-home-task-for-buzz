import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_work_test/model/calendar_model.dart';
import 'package:toast/toast.dart';

/// In this class we will keep our common function
/// That will user in different places in code

class Common {
  static List<String> weekList = ["S", "M", "T", "W", "T", "F", "S"];
  static List<CalendarModel> daysOfMonth = [];
  static List<CalendarModel> inActiveDays = [];

  /// This function make toast message
  /// Parameters : [BuildContext], String message
  static void showToastMessage(BuildContext context, String message) {
    Toast.show(message, context, duration: 2, gravity: Toast.BOTTOM);
  }

  /// Week List for week title
  static List getWeekList() {
    return weekList;
  }

  /// get the Month with days it could be 28, 30, 31
  static List<CalendarModel> getDaysOfMonth(int numberOfDays) {
    daysOfMonth.clear();
    for (int i = 0; i < numberOfDays; i++) {
      daysOfMonth.add(CalendarModel(day: (i + 1), isActive: true));
    }
    return daysOfMonth;
  }


  ///This function is responsible for giving us inactive days before month start
  static List<CalendarModel> getInActiveDaysBeforeMonth(int days) {
    inActiveDays.clear();
    switch (days) {
      case 1:
        {
          return getInActiveDays(30);
        }
        break;

      case 2:
        {
          return getInActiveDays(29);
        }
        break;

      case 3:
        {
          return getInActiveDays(28);
        }
        break;

      case 4:
        {
          return getInActiveDays(27);
        }
        break;

      case 5:
        {
          return getInActiveDays(26);
        }
        break;

      case 6:
        {
          return getInActiveDays(25);
        }
        break;

      case 7:
        {
          return getInActiveDays(24);
        }
        break;

      default:
        {
          return inActiveDays;
        }
    }
  }

  static List<CalendarModel> getInActiveDays(int startFrom) {
    for (int i = startFrom; i <= 30; i++) {
      inActiveDays.add(CalendarModel(day: i, isActive: false));
    }
    return inActiveDays;
  }

  ///This function is responsible for giving inactive days after month days
  static List<CalendarModel> getInactiveDaysAfterMonthDays(int completedNumberOfDays){
    inActiveDays.clear();
    int days=1;
    for (int i = completedNumberOfDays; i < 42; i++) {
      inActiveDays.add(CalendarModel(day: days, isActive: false));
      days++;
    }
    return inActiveDays;
  }
}
