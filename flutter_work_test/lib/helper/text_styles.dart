import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import "global.dart" as globals;

/// std font size
const double stdFontSize = 22;

/// text styles to all inputs and forms
class TextStyles {

  /// this style use for list text
  static TextStyle getNoteTextStyle(Color color) {
    return GoogleFonts.openSans(
        color: color, fontSize: 18);
  }

  /// this style use for list text
  static TextStyle getTitleStyle(Color color) {
    return GoogleFonts.openSans(
        color: color, fontWeight: FontWeight.bold, fontSize: stdFontSize);
  }

  /// get calendar days style
  static TextStyle getDaysStyle(Color color) {
    return GoogleFonts.openSans(
        color: color, fontWeight: FontWeight.normal, fontSize: stdFontSize);
  }




}
