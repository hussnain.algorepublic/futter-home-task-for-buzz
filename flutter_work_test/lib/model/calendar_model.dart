
class CalendarModel{
  int day;
  bool isActive;

  CalendarModel({this.day, this.isActive});

  void setDay(int day){
    this.day=day;
  }

  void setIsActive(bool isActive){
    this.isActive=isActive;
  }

  int get getDay=> day??0;

  bool get getIsActive=>isActive??false;

}