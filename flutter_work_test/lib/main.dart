import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_work_test/helper/text_styles.dart';
import 'helper/global.dart' as globals;
import 'package:flutter_work_test/helper/navigation_helper.dart';
import 'package:flutter_work_test/widgets/app_error_widget.dart';

void main() {

  /// Error widget if our app is crashed so this layout will show
  /// instead of that red screen
  ErrorWidget.builder = (errorDetails) {
    return AppErrorWidget(
      errorDetails: errorDetails,
    );
  };

  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Work Task',
      theme: ThemeData(
        primarySwatch: globals.kPrimaryColor,
      ),
      home: Splash(),
    );
  }
}

class Splash extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  /// splash timer
  void startTime() async {
    Timer(Duration(seconds: 1), navigateToCalendarView);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: globals.themColor,
      body:  Center(
        child:  Text(
          'Flutter Work Task',
          style: TextStyles.getTitleStyle(globals.backgroundColor),
        ),
      ),
    );
  }

  /// navigate to calender view
  void navigateToCalendarView(){
   NavigationManager.navigateToCalendarView(context);
  }


}