import 'package:date_util/date_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_work_test/helper/common.dart';
import 'package:flutter_work_test/model/calendar_model.dart';
import 'package:intl/intl.dart';
import 'package:flutter_work_test/helper/global.dart' as globals;
import 'package:mvc_application/controller.dart' show Color, ControllerMVC;


class CalendarController extends ControllerMVC{
  factory CalendarController() => _this ??= CalendarController._();
  List<CalendarModel> calendarList=[];

  int selectedIndex;

  CalendarController._(){
  return;
  }

  static CalendarController _this;

  /// set selected index
  void setSelectedIndex(int index){
    this.selectedIndex=index;
  }

  /// get selected index
  int get getSelectedIndex=> selectedIndex??0;

  /// This function will provides you monh and year detail
  String getYearAndMonth(){
    DateTime now = DateTime.now();
    String yaerAndMonth = DateFormat('MMMM y').format(now);
    return yaerAndMonth;
  }


  /// Create complete month list for calendar view
  void setCalendarList(){
    var dateUtility = DateUtil();
    DateTime date =new DateTime(DateTime.now().year, DateTime.now().month, 1);
    var numberOfDays = dateUtility.daysInMonth(DateTime.now().month, DateTime.now().year);

    calendarList.addAll(Common.getInActiveDaysBeforeMonth(date.weekday));

    calendarList.addAll(Common.getDaysOfMonth(numberOfDays));

    calendarList.addAll(Common.getInactiveDaysAfterMonthDays(calendarList.length));

    selectedIndex=DateTime.now().day+date.weekday-1;
  }

  /// get Item of calendar model for days and for status of days
  CalendarModel getCalendarModel(int index){
    if(calendarList.length>index){
      return calendarList.elementAt(index);
    }
  }


  /// get selected block color for calendar
  Color getCalendarBlockColor(int index){
    return index==getSelectedIndex?globals.themColor:Colors.transparent;
  }


  /// get calendar text color
  Color getCalendarStyleColor(int index){
    return index==getSelectedIndex?globals.backgroundColor:getCalendarModel(index).isActive?
    globals.blackColor:globals.inActiveDaysColor;
  }


}