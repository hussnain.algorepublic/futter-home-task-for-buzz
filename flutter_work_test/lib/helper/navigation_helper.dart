import 'package:flutter/material.dart';
import 'package:flutter_work_test/calendar/calendar_view.dart';

/// Class to manage navigation across the app from one screen to another.
///
/// This class include several methods which are redirecting users
/// to another screen.
class NavigationManager {

  /// Navigate to Merchant List View
  /// Parameters : [BuildContext]
  static void navigateToCalendarView(BuildContext context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => CalendarView()),
        (route) => false);
  }


}
